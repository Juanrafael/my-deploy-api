FROM adoptopenjdk/openjdk11:latest
EXPOSE 8090
ADD /target/my-deploy-api-0.0.1-SNAPSHOT.jar my-deploy-api-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","my-deploy-api-0.0.1-SNAPSHOT.jar"]