package com.deploy.api.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deploy.api.demo.domain.dao.UsuarioDAO;
import com.deploy.api.demo.service.UsuarioService;

@RestController
public class UsuarioController {
    
    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/users")
    public ResponseEntity<List<UsuarioDAO>> getUsers(){
        return new ResponseEntity(usuarioService.getUsers(),HttpStatus.OK);
    }
}
