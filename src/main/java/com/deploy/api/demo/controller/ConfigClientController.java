package com.deploy.api.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class ConfigClientController {
    @Value("${sample.property: Default property}")
    private String property;

    @Value("${sample.filter: Default filter}")
    private String filter;

    @Value("${sample.enviroment: Default filter}")
    private String enviroment;

    @Value("${mysql.host: Default host}")
    private String host;

    @Value("${mysql.port: Default port}")
    private String port;

    @Value("${mysql.database: Default host}")
    private String database;

    @Value("${mysql.user: Default user}")
    private String user;

    @Value("${mysql.password: Default password}")
    private String password;

    @GetMapping("/showConfig")
    public String showConfig(){
        String configInfo = "Property: " + property //
        + "<br/>Filter=" + filter //
        + "<br/>Enviroment=" + enviroment 
        + "<br/>Host=" + host 
        + "<br/>Port=" + port         
        + "<br/>Database=" + database 
        + "<br/>User=" + user 
        + "<br/>Password=" + password ;
        

    return configInfo;
    }
}
