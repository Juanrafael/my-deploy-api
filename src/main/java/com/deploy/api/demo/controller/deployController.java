package com.deploy.api.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deploy.api.demo.domain.dao.MessageDao;
import com.deploy.api.demo.service.MessageService;

@RestController
public class deployController {
    
    @Autowired
    private MessageService messageService;

    @GetMapping("/showMessage")
    public String showMessage(){
        return "Hola Mundo";
    }

    @PostMapping("/getMessages")
    public List<MessageDao> getMessages(){
        return messageService.getMessages();
    }

}
