package com.deploy.api.demo.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Repository;

import com.deploy.api.demo.domain.dao.MessageDao;

@Repository
public class MessageRepo {
    public List<MessageDao> getMessages(){
        List<MessageDao> messageDaos = new ArrayList<>();
        Stream.iterate(1,n->n+1).limit(5)
        .forEach(x->messageDaos.add(MessageDao.builder().id(x.longValue())
        .message("Message".concat(x.toString())).build())
        );

        return messageDaos;
    }
}
