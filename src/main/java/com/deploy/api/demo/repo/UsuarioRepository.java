package com.deploy.api.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.deploy.api.demo.domain.dao.UsuarioDAO;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioDAO,Integer>{
    @Query(value = "SELECT * FROM restapi.Usuario u", nativeQuery=true)
    List<UsuarioDAO> findAll();
    
}
