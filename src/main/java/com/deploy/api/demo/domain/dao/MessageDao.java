package com.deploy.api.demo.domain.dao;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MessageDao {
    private Long id;
    private String message;
}
