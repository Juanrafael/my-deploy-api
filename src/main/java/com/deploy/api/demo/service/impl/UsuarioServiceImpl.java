package com.deploy.api.demo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deploy.api.demo.domain.dao.UsuarioDAO;
import com.deploy.api.demo.model.dto.UsuarioDTO;
import com.deploy.api.demo.repo.UsuarioRepository;
import com.deploy.api.demo.service.UsuarioService;


@Service
public class UsuarioServiceImpl implements UsuarioService{
    
    @Autowired
    private UsuarioRepository usuarioRepo;

    @Autowired
	private ModelMapper mapper;
    
    public List<UsuarioDTO> getUsers(){

        List<UsuarioDAO> lUsuarioDTOs = usuarioRepo.findAll();
        
        return usuarioRepo.findAll().stream()
        //.map(usuario-> mapper.map(usuario, UsuarioDTO.class))
        .map(usuario-> UsuarioDTO.builder()
            .id(usuario.getId())
            .password(usuario.getPassword())
            .estado(usuario.getEstado())
            .usuario(usuario.getUsuario())
            .build())
        .collect(Collectors.toList());
    }
}
