package com.deploy.api.demo.service;

import java.util.List;

import com.deploy.api.demo.model.dto.UsuarioDTO;

public interface UsuarioService {
    public List<UsuarioDTO> getUsers();
}
