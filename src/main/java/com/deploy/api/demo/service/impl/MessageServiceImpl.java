package com.deploy.api.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deploy.api.demo.domain.dao.MessageDao;
import com.deploy.api.demo.repo.MessageRepo;
import com.deploy.api.demo.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService{
        
    @Autowired
    private MessageRepo messageRepo;

    public List<MessageDao> getMessages(){
        return messageRepo.getMessages();
    }
}
