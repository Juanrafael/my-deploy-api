package com.deploy.api.demo.service;

import java.util.List;

import com.deploy.api.demo.domain.dao.MessageDao;

public interface MessageService {
    List<MessageDao> getMessages();   
}
